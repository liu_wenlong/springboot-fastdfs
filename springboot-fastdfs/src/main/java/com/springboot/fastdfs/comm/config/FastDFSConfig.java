package com.springboot.fastdfs.comm.config;

import java.net.InetSocketAddress;

import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient1;
import org.csource.fastdfs.StorageServer;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerGroup;
import org.csource.fastdfs.TrackerServer;

/**
 * 
 * @desc FastDFS 配置
 * @author chay
 * @version 2.0.0
 * @date 2018-07-12
 */
public class FastDFSConfig {
	
	public static TrackerClient tracker = null;  
	public static TrackerServer trackerServer = null;  
	public static StorageServer storageServer = null;  
	public static StorageClient1 client = null;   
	
	/** 客户端启动传参-fastDFS跟踪器地址 */
	public static String[] CLIENT_TRACKER_ADDRESS = null;
	/** 客户端启动传参-fastDFS跟踪器端口 */
	public static int[] CLIENT_TRACKER_PORT = null;
	
	/** fdfs_client.conf配置文件-fastDFS跟踪器地址 */
	public static String[] CONF_TRACKER_ADDRESS = null;
	/** fdfs_client.conf配置文件-fastDFS跟踪器端口 */
	public static int[] CONF_TRACKER_PORT = null;
	
	/**
	 * 初始化连接客户端(优先采用启动时传进来的参数且与配置文件互斥)
	 * 1.读取配置文件方式
	 * 2.根据界面传进来的参数进行初始化
	 * ps：两者没做兼容
	 * @return
	 */
	public static StorageClient1 initClient(){
		try {
			// 1.服务器连接初始化
			FastDFSConfig.reloadTrackerServer();
            // 2. 连接客户端初始化
			FastDFSConfig.tracker = new TrackerClient();  
			FastDFSConfig.trackerServer = tracker.getConnection();  
			FastDFSConfig.storageServer = null;  
			FastDFSConfig.client = new StorageClient1(trackerServer, storageServer);  
		    return client;
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return null;
	}
	/**
	 * 服务器连接初始化
	 */
	public static void reloadTrackerServer(){
		
		// 1.启动客户端时没有传入相应的参数
		try {
			if(FastDFSConfig.CLIENT_TRACKER_ADDRESS !=null && FastDFSConfig.CLIENT_TRACKER_PORT !=null) {
				if(FastDFSConfig.CLIENT_TRACKER_ADDRESS.length>0 && 
						FastDFSConfig.CLIENT_TRACKER_PORT.length>0){
					FastDFSConfig.ClientGlobalInit();
					InetSocketAddress[] tracker_servers = new InetSocketAddress[FastDFSConfig.CLIENT_TRACKER_ADDRESS.length];  
					for(int i=0;i<FastDFSConfig.CLIENT_TRACKER_ADDRESS.length;i++){
						tracker_servers[i] = new InetSocketAddress(FastDFSConfig.CLIENT_TRACKER_ADDRESS[i], FastDFSConfig.CLIENT_TRACKER_PORT[i]);
					}
				    ClientGlobal.setG_tracker_group(new TrackerGroup(tracker_servers));
				}
			}else{	// 2.当客户端不传入参数的时候读取配置文件中的配置
				String fdfsConf = FastDFSConfig.class.getResource("/fdfs_client.conf").toString();
				fdfsConf = fdfsConf.substring(fdfsConf.indexOf("/") + 1);
				ClientGlobal.init(fdfsConf);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
	}
	/**
	 * 客户端动态传参是默认参数
	 */
	public static void ClientGlobalInit(){
		//连接超时的时限，单位为毫秒  
		ClientGlobal.setG_connect_timeout(2000);  
		//网络超时的时限，单位为毫秒  
		ClientGlobal.setG_network_timeout(30000);  
		ClientGlobal.setG_anti_steal_token(false);  
		//字符集  
		ClientGlobal.setG_charset("UTF-8");  
		ClientGlobal.setG_secret_key("FastDFS1234567890"); 
		//HTTP访问服务的端口号    
		ClientGlobal.setG_tracker_http_port(8080);  
	}
	
	
	
}
